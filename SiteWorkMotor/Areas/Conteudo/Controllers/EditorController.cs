﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.Core;
using Core.Security;
using SiteWorkMotor.Helper;
using SiteWorkMotor.ViewModel;

namespace SiteWorkMotor.Areas.Conteudo.Controllers
{
    public class EditorController : Controller
    {
        private readonly PostCore _postCore = new PostCore();
        // GET: Conteudo/Editor
        public ActionResult Index(int id)
        {
            if (new UsuarioAutenticado().Usuario == null)
                return RedirectToAction("Index", "LoginConteudo", new {area = "Conteudo"});
            var post = _postCore.PorId(id);
            if (post != null)
            {
                if (post.PathMiniatura == null)
                {
                    return View(new EditorConteudoViewModel()
                    {

                        PalavrasChave = post.PALAVRASCHAVESPOSTS.Where(y => y.Habilitado == true)
                            .Select(x => x.Descricao)
                            .ToList(),
                        Titulo = post.Titulo,
                        Texto = PostFileHelper.RecuperarTextoPost(post.PathNoticia),
                        Resumo = post.Resumo,
                        PathMiniatura = "Adicionar miniatura"
                    });
                }
                string[] pathMiniatura = post.PathMiniatura.Split(new char[] { '\\' });

                return View(new EditorConteudoViewModel()
                {

                    PalavrasChave = post.PALAVRASCHAVESPOSTS.Where(y => y.Habilitado == true).Select(x => x.Descricao)
                        .ToList(),
                    Titulo = post.Titulo,
                    Texto = PostFileHelper.RecuperarTextoPost(post.PathNoticia),
                    Resumo = post.Resumo,
                    PathMiniatura = pathMiniatura[pathMiniatura.Length - 1]
                });
                
                
            }
            return View(new EditorConteudoViewModel());
        }

        [HttpPost]
        [ValidateInput(false)]
        public int Formulario(EditorConteudoViewModel editor)
        {
            int postId;
            switch (editor.Tipo)
            {
                case 1:
                    postId = Salvar(editor);
                    if (postId != 0)
                    {
                        Session["PostId"] = postId;
                    }
                    return postId;

                case 2:
                    postId = Publicar(editor);
                    if (postId != 0)
                    {
                        Session["PostId"] = postId;
                    }

                    return postId;
            }
            return 0;
        }

        private int Salvar(EditorConteudoViewModel model)
        {
            if (!ModelState.IsValid) return 0;
            var usu = 0;
            try
            {
                usu = Convert.ToInt32(System.Web.HttpContext.Current.User.Identity.Name);
            }
            catch { }

            Post post = SalvarPost(new Post()
            {
                Id = model.Id,
                Titulo = model.Titulo,
                UsuarioId = usu,
                DataCriacao = DateTime.Now,
                PathNoticia = "",
                StatusId = 1,
                Resumo = model.Resumo,
                PALAVRASCHAVESPOSTS = model.PalavrasChave
                    .Select(p => new PalavraChave() { Descricao = p, Habilitado = true }).ToList(),
            }, model.Texto);

            return post.Id;
        }

        [ValidateInput(false)]
        private int Publicar(EditorConteudoViewModel model)
        {
            if (!ModelState.IsValid) return 0;
            var usu = 0;
            try
            {
                usu = Convert.ToInt32(System.Web.HttpContext.Current.User.Identity.Name);
            }
            catch { }

            Post post = SalvarPost(new Post()
            {
                Id = model.Id,
                Titulo = model.Titulo.ToString(),
                UsuarioId = usu,
                DataCriacao = DateTime.Now,
                DataPublicacao = DateTime.Now,
                PathNoticia = "",
                StatusId = 1,
                Resumo = model.Resumo.ToString(),
                PALAVRASCHAVESPOSTS = model.PalavrasChave
                    .Select(p => new PalavraChave() { Descricao = p, Habilitado = true }).ToList(),
            }, model.Texto.ToString());

            return post.Id;
        }

        [HttpGet]
        public ActionResult Publicar(int id)
        {
            var p = _postCore.PorId(id);
            p.StatusId = 2;
            p.DataPublicacao = DateTime.Now;
            _postCore.Atualizar(p);
            return RedirectToAction("Index", "DashboardPublicacoes", new { area = "Conteudo" });
        }

        [HttpGet]
        public ActionResult Despublicar(int id)
        {
            var p = _postCore.PorId(id);
            p.StatusId = 3;
            _postCore.Atualizar(p);
            return RedirectToAction("Index", "DashboardPublicacoes", new { area = "Conteudo" });
        }

        [HttpGet]
        public ActionResult Deletar(int id)
        {
            var p = _postCore.PorId(id);
            p.StatusId = 4;
            _postCore.Atualizar(p);
            return RedirectToAction("Index", "DashboardPublicacoes", new { area = "Conteudo" });
        }

        [ValidateInput(false)]
        private Post SalvarPost(Post model, string texto)
        {
            Post post;
            if (model.Id == 0) post = _postCore.Salvar(model);
            else
            {
                var postBd = _postCore.PorId(model.Id);
                postBd.Titulo = model.Titulo;
                postBd.Resumo = model.Resumo;
                var palavras = postBd.PALAVRASCHAVESPOSTS.ToList();

                if (palavras.Count >= model.PALAVRASCHAVESPOSTS.Count)
                {
                    foreach (var p in palavras)
                    {
                        if (!model.PALAVRASCHAVESPOSTS.Select(x => x.Descricao).Contains(p.Descricao))
                        {
                            postBd.PALAVRASCHAVESPOSTS.FirstOrDefault(x => x.Id == p.Id).Habilitado = false;
                        }
                        else
                        {
                            postBd.PALAVRASCHAVESPOSTS.FirstOrDefault(x => x.Id == p.Id).Habilitado = true;
                        }
                    }
                }
                else
                {
                    foreach (var m in model.PALAVRASCHAVESPOSTS)
                    {
                        if (!palavras.Select(p => p.Descricao).Contains(m.Descricao))
                        {
                            postBd.PALAVRASCHAVESPOSTS.Add(m);
                        }
                        else
                        {
                            postBd.PALAVRASCHAVESPOSTS.FirstOrDefault(x => x.Descricao == m.Descricao).Habilitado = true;
                        }
                    }
                }
                post = _postCore.Atualizar(postBd);
            }

            var caminho = PostFileHelper.CriarCaminhoArquivo(texto, post.Id);
            _postCore.SalvarCaminho(post.Id, caminho);
            return post;
        }

        public bool SalvarMiniatura()
        {
            try
            {
                var id = Convert.ToInt32(Session["PostId"]);
                var files = Request.Files;
                var postBd = _postCore.PorId(id);
                if (files.Count != 0)
                {
                    for (int i = 0; i < files.Count; i++)
                    {
                        var file = files[i];
                        string fname = "";

                        if (Request.Browser.Browser.ToUpper() == "IE" ||
                            Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            if (file != null)
                            {
                                var testfiles = file.FileName.Split(new char[] { '\\' });
                                fname = testfiles[testfiles.Length - 1];
                            }
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        var path = Server.MapPath("~/Content/ImagensPosts/");/*Miniatura_" + id */
                        Directory.CreateDirectory(path);
                        fname = Path.Combine(path, fname);
                        file.SaveAs(fname);
                        postBd.PathMiniatura = fname;
                        _postCore.Atualizar(postBd);
                        
                        return true;
                    } 
                } else if (postBd.PathMiniatura != null)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        [HttpPost]
        public void SalvarImagem()
        {
            var filesBase = Request.Files;
            if (filesBase.Count > 0)
            {
                try
                {
                    var files = filesBase;
                    var arquivos = new List<string>();
                    for (int i = 0; i < files.Count; i++)
                    {
                        var file = files[i];
                        string fname = "";

                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            if (file != null)
                            {
                                var testfiles = file.FileName.Split(new char[] { '\\' });
                                fname = testfiles[testfiles.Length - 1];
                            }
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        var path = Server.MapPath("~/Content/ImagensPosts/");
                        Directory.CreateDirectory(path);
                        fname = Path.Combine(path, fname);
                        file.SaveAs(fname);
                        arquivos.Add(fname);
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }

        public PartialViewResult ImagensParaPosts()
        {
            return PartialView();
        }
    }
}