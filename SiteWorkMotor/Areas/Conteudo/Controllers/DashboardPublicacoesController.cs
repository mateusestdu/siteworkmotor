﻿using System;
using System.Linq;
using System.Web.Mvc;
using Core;
using Core.Core;
using Core.Security;
using SiteWorkMotor.Helper;
using SiteWorkMotor.ViewModel;

namespace SiteWorkMotor.Areas.Conteudo.Controllers
{
    public class DashboardPublicacoesController : Controller
    {
        private readonly PostCore _postCore = new PostCore();
        // GET: Conteudo/DashboardPublicacoes
        public ActionResult Index()
        {
            if ((new UsuarioAutenticado().Usuario == null)) return RedirectToAction("Index", "LoginConteudo",new{area="Conteudo"});
            return View(_postCore.TodosSemStatus().Select(Istanciar).ToList());
        }

        private PublicaoViewModel Istanciar(Post post)
        {
            return new PublicaoViewModel()
            {
                Titulo = post.Titulo,
                DataPublicacao = post.DataPublicacao ?? new DateTime(),
                Id = post.Id,
                Status = post.STATUSPOSTS.Descricao,
                Url = PostHelper.UrlPorPalavraChave(post.PALAVRASCHAVESPOSTS.Select(x=>x.Descricao).ToList())
            };
        }
    }
}