﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Core;
using Core.Security;
using Domain.Security;
using SiteWorkMotor.ViewModel;

namespace SiteWorkMotor.Areas.Conteudo.Controllers
{
    public class LoginConteudoController : Controller
    {
        private readonly UsuarioCore _usuarioCore = new UsuarioCore();
        private readonly FormsAuthenticationService _authentication = new FormsAuthenticationService(System.Web.HttpContext.Current);

        public ActionResult Index()
        {
            if (new UsuarioAutenticado().Usuario != null) return RedirectToAction("Index", "DashboardPublicacoes", new { area = "Conteudo" });
            return View();
        }

        [HttpPost]
        public ActionResult Index(UsuarioViewModel usuario)
        {
            if (!ModelState.IsValid)
            {
                return View("Index", usuario);
            }
            if (_usuarioCore.Autenticar(usuario.Login, usuario.Senha,2))
            {
                var u = _usuarioCore.PorLogin(usuario.Login);
                _authentication.SignIn(u, false);
                return RedirectToAction("Index", "DashboardPublicacoes");
            }
            @ViewBag.mensagem = "Usuário não autenticado.";
            return View("Index", usuario);
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            _authentication.SignOut();
            return RedirectToAction("Index");
        }
    }
}