﻿using System.Web.Mvc;

namespace SiteWorkMotor.Areas.Conteudo
{
    public class ConteudoAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "Conteudo";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Conteudo_default",
                "Conteudo/{controller}/{action}/{id}",
                new { controller = "LoginConteudo", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}