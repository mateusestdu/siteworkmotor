﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteWorkMotor.ViewModel
{
    public class ContatoCvViewModel
    {
        [Display(Name = "Mensagem", Description = "Digite sua mensagem")]
        [Required(ErrorMessage = "A mensagem é obrigatória.")]
        public string MensagemCv { get; set; }

        [Display(Name = "Nome Completo", Description = "Nome e Sobrenome.")]
        [Required(ErrorMessage = "O nome completo é obrigatório.")]
        [RegularExpression(@"^[a-zA-Z''-'\s]{1,40}$", ErrorMessage = "Números e caracteres especiais não são permitidos no nome.")]
        public string NomeCv { get; set; }

        [Required(ErrorMessage = "Campo obrigatório")]
        [EmailAddress(ErrorMessage = "E-mail inválido")]
        public string EmailCv { get; set; }

        [Required(ErrorMessage = "Campo obrigatório")]
        public string TelefoneCv { get; set; }

    }
}