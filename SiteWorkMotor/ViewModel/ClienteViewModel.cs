﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteWorkMotor.ViewModel
{
    public class ClienteViewModel
    {
        public int Id { get; set; }

        [Required]
        [System.Web.Mvc.Remote("CpfCnpjValido", "Free", HttpMethod = "POST", ErrorMessage = "CPF/CNPJ inválido.")]
        [Display(Name = "CPF/CNPJ")]
        public string CpfCnpj { get; set; }

        [Required]
        [Display(Name = "NOME/RAZÃOSOCIAL")]
        public string RazaoSocial { get; set; }

        [Required]
        [Display(Name = "EMAIL")]
        [EmailAddress(ErrorMessage = "Email inválido")]
        public string Email { get; set; }

        [Required]
        [System.Web.Mvc.Remote("TelefoneValido", "Free", HttpMethod = "POST", ErrorMessage = "TELEFONE inválido.")]
        [Display(Name = "TELEFONE")]
        public string Telefone { get; set; }

        [Required]
        [System.Web.Mvc.Remote("CepValido", "Free", HttpMethod = "POST", ErrorMessage = "CEP inválido.")]
        [Display(Name = "CEP")]
        public string Cep { get; set; }

        [Required]
        [Display(Name = "DISTRIBUIDOR")]
        public int PatrocinadorId { get; set; }

        public int IndicacaoId { get; set; }
    }
}