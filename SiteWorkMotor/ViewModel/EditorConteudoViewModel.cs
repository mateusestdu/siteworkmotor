﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteWorkMotor.ViewModel
{
    public class EditorConteudoViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Titulo", Description = "Titulo da notícia")]
        [Required(ErrorMessage = "O titulo da notícia é obrigatório.")]
        public string Titulo { get; set; }

        [Display(Name = "Resumo", Description = "Resumo da notícia")]
        [Required(ErrorMessage = "O Resumo da notícia é obrigatório.")]
        public string Resumo { get; set; }

        [Display(Name = "Texto", Description = "Texto da notícia")]
        [Required(ErrorMessage = "O Texto da notícia é obrigatório.")]
        public string Texto { get; set; }

        [Display(Name = "Palavras chave", Description = "Palavras chave da notícia")]
        [Required(ErrorMessage = "As palavras chaves da notícia é obrigatório.")]
        public List<string> PalavrasChave{ get; set; }

        public int Tipo { get; set; }

        public string PathMiniatura { get; set; }
    }
}