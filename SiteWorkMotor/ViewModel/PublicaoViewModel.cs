﻿using System;

namespace SiteWorkMotor.ViewModel
{
    public class PublicaoViewModel
    {
        public int Id { get; set; }
        public string Titulo{ get; set; }
        public DateTime DataPublicacao { get; set; }
        public string Status { get; set; }
        public string Url { get; set; }
    }
}