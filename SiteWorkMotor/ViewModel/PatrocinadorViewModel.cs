﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteWorkMotor.ViewModel
{
    public class PatrocinadorViewModel
    {
        public int Id { get; set; }
        public string Cnpj { get; set; }
        public string RazaoSocial { get; set; }
        public string LinkExe { get; set; }
        public string NomeFantasia { get; set; }
        public string NomeExe { get; set; }
        public Nullable<System.DateTime> DataExe { get; set; }
        public string LinkAtualizador { get; set; }
        public string NomeAtualizador { get; set; }
        public Nullable<System.DateTime> DataAtualizador { get; set; }
    }
}