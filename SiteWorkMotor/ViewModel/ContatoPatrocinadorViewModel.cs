﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteWorkMotor.ViewModel
{
    public class ContatoPatrocinadorViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "NOME/RAZÃOSOCIAL")]
        public string NomeRazaoSocial { get; set; }

        [Required]
        [Display(Name = "TELEFONE")]
        public string Telefone { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Email inválido")]
        [Display(Name = "EMAIL")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "MENSAGEM")]
        public string Mensagem { get; set; }
    }
}