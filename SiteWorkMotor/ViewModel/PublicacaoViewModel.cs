﻿
using System;
using System.Collections.Generic;

namespace SiteWorkMotor.ViewModel
{
    public class PublicacaoViewModel
    {
        public int Id { get; set; }
        public DateTime PublicadoEm { get; set; }
        public string TextoPublicacao { get; set; }
        public string TituloTexto { get; set; }
        public string Resumo { get; set; }
        public List<string> PalavrasChave { get; set; }
        public string Url { get; set; }
        public string PathMiniatura { get; set; }
    }
}