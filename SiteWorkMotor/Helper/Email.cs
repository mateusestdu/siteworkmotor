﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace SiteWorkMotor.Helper
{
    public class Email
    {
        public List<string> Destinatario { get; set; }
        public string Conteudo { get; set; }
        public string Assunto { get; set; }
        public static string RemetenteEmail { get; set; }
        public static string RemetenteNome { get; set; }

        public Email(List<string> destinatario, string remetenteEmail, string remetenteNome, string conteudo, string assunto)
        {
            Destinatario = destinatario;
            RemetenteEmail = remetenteEmail;
            RemetenteNome = remetenteNome;
            Conteudo = conteudo;
            Assunto = assunto;
        }

        public bool SendEmail()
        {
            //Define os dados do e-mail
            var nomeRemetente = RemetenteNome;
            var emailRemetente = RemetenteEmail;

            //var emailDestinatario = Destinatario.FirstOrDefault();
            //string emailComCopia = "adilson.junior@pequenasempresas.net";

            var assuntoEmail = Assunto;
            var conteudoEmail = Conteudo;

            //Cria objeto com dados do e-mail.
            var objEmail = new MailMessage();

            //Define o Campo From e ReplyTo do e-mail.
            objEmail.From = new MailAddress(nomeRemetente + "<" + emailRemetente + ">");

            if (Destinatario != null)
                foreach (var d in Destinatario)
                {
                    objEmail.To.Add(d);
                }
            //Define os destinatários do e-mail.

            //Enviar cópia para.
            //objEmail.CC.Add(emailComCopia);

            //Define a prioridade do e-mail.
            objEmail.Priority = MailPriority.High;

            //Define o formato do e-mail HTML (caso não queira HTML alocar valor false)
            objEmail.IsBodyHtml = true;

            //Define título do e-mail.
            objEmail.Subject = assuntoEmail;

            //Define o corpo do e-mail.
            objEmail.Body = conteudoEmail;

            //Para evitar problemas de caracteres "estranhos", configuramos o charset para "ISO-8859-1"
            objEmail.SubjectEncoding = Encoding.GetEncoding("UTF-8");
            objEmail.BodyEncoding = Encoding.GetEncoding("UTF-8");

            // Caso queira enviar um arquivo anexo
            //Caminho do arquivo a ser enviado como anexo
            //string arquivo = Server.MapPath("arquivo.jpg");
            // Ou especifique o caminho manualmente
            //string arquivo = @"e:\home\LoginFTP\Web\arquivo.jpg";
            //var arquivo = @caminhoAnexo;
            // Cria o anexo para o e-mail
            //var anexo = new Attachment(arquivo, System.Net.Mime.MediaTypeNames.Application.Octet);
            // Anexa o arquivo a mensagemn
            //objEmail.Attachments.Add(anexo);

            //Cria objeto com os dados do SMTP
            var objSmtp = new SmtpClient();

            //Alocamos o endereço do host para enviar os e-mails, localhost(recomendado) 
            objSmtp.Host = "smtplw.com.br";
            objSmtp.Port = 587;
            objSmtp.UseDefaultCredentials = false;
            objSmtp.EnableSsl = true;
            objSmtp.Credentials = new NetworkCredential("pequenasempresas", "KmzEytOK5144");
            //Enviamos o e-mail através do método .send()
            try
            {
                objSmtp.Send(objEmail);
                return true;
            }
            catch (Exception e)
            {
                var a = e.Message;
                return false;
            }
            finally
            {
                //excluímos o objeto de e-mail da memória
                objEmail.Dispose();
                //anexo.Dispose();
            }
        }
    }
}
