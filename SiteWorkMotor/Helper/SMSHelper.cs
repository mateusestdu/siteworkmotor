﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Core;
using Core.Core;

namespace SiteWorkMotor.Helper
{
    public class SMSHelper
    {
        private static void WebPost(Uri uri, string data)
        {
            var request = (HttpWebRequest)WebRequest.Create(uri);

            request.Method = "POST";
            request.ContentType = "text/plain;charset=utf-8";

            var bytes = new UTF8Encoding().GetBytes(data);

            request.ContentLength = bytes.Length;
            request.GetRequestStream().Write(bytes, 0, bytes.Length);
        }

        public bool EnviarSmsPersonalizado(Cliente cliente, string chave)
        {
            try
            {
                var link = "https://sms.comtele.com.br/api/";
                var token = "311246a0-6841-4dbd-aa24-14b35a9705f5";
                var send = "/sendmessage?sender={remetente}&receivers={destinatario}&content={mensagem}";
                var url = link + token + send;

                url = url
                    .Replace("{remetente}", "61448")
                    .Replace("{destinatario}", cliente.Telefone.Trim())
                    .Replace("{mensagem}", "Use o seguinte código para validar seu download do WorkMotor Free: " + chave);

                WebPost(new Uri(url), string.Empty);

                new SmsHistoricoCore().Inserir(new SmsHistorico { ClienteId = cliente.Id, Celular = cliente.Telefone.Trim(), ChaveSeguranca = chave, DataEnvio = DateTime.Now });

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}