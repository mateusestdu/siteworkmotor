﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using SiteWorkMotor.ViewModel;

namespace SiteWorkMotor.Helper
{
    public static class PostHelper
    {
        public static string UrlPorPalavraChave(List<string> descricao)
        {
            var s = "";
            foreach (var d in descricao)
            {
                s += d + "-";
            }
            s = s.Substring(0, s.Length - 1);
            return s;
        }
        public static PublicacaoViewModel ConverterPost(Post post)
        {
            return new PublicacaoViewModel()
            {
                PalavrasChave = post.PALAVRASCHAVESPOSTS.Select(x => x.Descricao).ToList(),
                TituloTexto = post.Titulo,
                PublicadoEm = post.DataPublicacao ?? post.DataCriacao ?? new DateTime(),
                Resumo = post.Resumo,
                TextoPublicacao = post.PathNoticia,
                Id = post.Id,
                Url = UrlPorPalavraChave(post.PALAVRASCHAVESPOSTS.Select(x => x.Descricao).ToList()),
                PathMiniatura = post.PathMiniatura
            };
        }

    }
}