﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Core;
using RestSharp;
using RestSharp.Authenticators;

namespace SiteWorkMotor.Helper
{
    public class EmailFreeHelper
    {
        public void EnviarEmailCadastro(Cliente cli, Patrocinador pat)
        {
            //Define os dados do e-mail
            var destinatario = "sitefree@pequenasempresas.net";
            var copia = "";

            copia = string.IsNullOrEmpty(pat.Email) ? "murilo.lourenco@pequenasempresas.net" : pat.Email;

            var assunto = "Cadastro Workmotor Free";

            var conteudo = "Workmotor Free <br><br> Cadastro para baixar o Workmotor Free realizado com os seguintes dados: <br><br> Nome/RazãoSocial: " + cli.NomeRazaoSocial +
                           "<br> CPF/CNPJ: " + cli.CpfCnpj +
                           "<br> Email: " + cli.Email +
                           "<br> Telefone: " + cli.Telefone +
                           "<br> Patrocinador: " + pat.RazaoSocial +
                           "<br><br> *Obs: Dados que serão solicitados na instalação do Workmotor Free deve ser os mesmos apresentados acima. <br><br> Muito Obrigado !<br>";

            var response = WebPost(destinatario, copia, assunto, conteudo);
        }
        public void EnviarEmailSegCadastro(Cliente cli, Patrocinador pat)
        {
            //Define os dados do e-mail
            var destinatario = "sitefree@pequenasempresas.net";
            var copia = "";
            var assunto = "Cadastro Workmotor Free";
            var conteudo = "Workmotor Free " +
                           "<br><br>" +
                           " Cadastro para baixar o Workmotor Free realizado com os seguintes dados:" +
                           " <br><br> " +
                           "Nome/RazãoSocial: " + cli.NomeRazaoSocial +
                           "<br> CPF/CNPJ: " + cli.CpfCnpj +
                           "<br> Email: " + cli.Email +
                           "<br> Telefone: " + cli.Telefone +
                           "<br> Patrocinador: " + pat.RazaoSocial;

            var response = WebPost(destinatario, copia, assunto, conteudo);
        }
        public void EnviarEmailPriCadastro(Cliente cli)
        {
            //Define os dados do e-mail
            var destinatario = "sitefree@pequenasempresas.net";
            var copia = "";
            //copia = string.IsNullOrEmpty(pat.Email) ? "murilo.lourenco@pequenasempresas.net" : pat.Email;
            var assunto = "Novo Cadastro Workmotor Free";
            var conteudo = "Workmotor Free <br><br> Foi realizado o cadastrado do WorkMotor Free com os seguintes dados;<br>" +
                           "<br> Email: " + cli.Email +
                           "<br> Telefone: " + cli.Telefone;

            var response = WebPost(destinatario, copia, assunto, conteudo);
        }

        public void EnviarEmailUpgrade(Cliente cli, Patrocinador pat)
        {
            //Define os dados do e-mail
            var destinatario = "upgrade@workmotor.com.br";
            //var destinatario = "murilo.lfs@gmail.com";

            var assunto = "Upgrade Workmotor Free";

            var conteudo = "Workmotor Free <br><br> Cliente deseja realizar um upgrade de versão. Segue abaixo seus dados para contato:" +
                           "<br><br> Nome/Razão Social: " + cli.NomeRazaoSocial +
                           "<br> Endereço: " + cli.Rua + ", CEP:" + cli.Cep + ", " + cli.Bairro + ", " + cli.Cidade + " - " + cli.Estado +
                           "<br> Email: " + cli.Email +
                           "<br> Telefone: " + cli.Telefone +
                           "<br> Patrocinador: " + pat.RazaoSocial +
                           "<br><br>";

            var response = WebPost(destinatario, assunto, conteudo);
        }

        public bool EnviarEmailContato(LogContatoPatrocinador contato)
        {
            //Define os dados do e-mail
            var destinatario = "patrocinadores@workmotor.com.br";

            var assunto = "Contato Workmotor Free";

            var conteudo = "Workmotor Free <br><br> " + contato.NomeRazaoSocial +
                           " (email: " + contato.Email +
                           " telefone: " + contato.Telefone +
                           ") se interessou em ser um de nossos patrocinadores e nos enviou a seguinte mensagem: <br><br>" + contato.Mensagem +
                           ".";

            var response = WebPost(destinatario, assunto, conteudo);

            return (response.StatusCode.Equals(HttpStatusCode.OK));
        }

        public bool EnviarEmailDownload(Cliente cliente)
        {
            //Define os dados do e-mail
            var destinatario = cliente.Email;

            var assunto = "";

            var conteudo = "<style> body{ font-family: Arial; } </style>" +
                           "<h1>Olá, " + cliente.NomeRazaoSocial + "</h1>" +
                           "<p>Obrigado por realizar o cadastro do Workmotor.</p>" +
                           "<p>Para realizar o download do WorkMotor clique no botão abaixo;</p>" +
                           "<a href=https://www.workmotor.com.br/free/baixararquivo/?idPatrocinador=" + cliente.PatrocinadorId + "><button type='button' class='btn' style='color: #fff; background-color: #5cb85c; border-color: #5cb85c;'>Baixar WorkMotor</button></a>" +
                           "<p>Se o botão não funcionar, copie e cole o seguinte link em seu navegador:</p>" +
                           "https://www.workmotor.com.br/free/baixararquivo/?idPatrocinador=" + cliente.PatrocinadorId;

            var response = WebPost(destinatario, assunto, conteudo);

            return (response.StatusCode.Equals(HttpStatusCode.OK));
        }

        public static IRestResponse WebPost(string destinatario, string copia, string assunto, string body)
        {
            var client = new RestClient
            {
                BaseUrl = "https://api.mailgun.net/v3",
                Authenticator = new HttpBasicAuthenticator("api", "key-3132beb8fa08a434ae0d8c3d7f022e99")
            };

            var request = new RestRequest();
            request.AddParameter("domain", "www.workmotor.com.br", ParameterType.UrlSegment);
            request.Resource = "www.workmotor.com.br/messages";
            request.AddParameter("from", "Workmotor Free <contato@workmotor.com.br>");
            request.AddParameter("to", destinatario);
            //request.AddParameter("cc", copia);
            //request.AddParameter("cc", "jackson.oliveira@pequenasempresas.net");

            request.AddParameter("subject", assunto);
            request.AddParameter("html", body);
            request.Method = Method.POST;
            return client.Execute(request);
        }

        public static IRestResponse WebPost(string destinatario, string assunto, string body)
        {
            var client = new RestClient
            {
                BaseUrl = "https://api.mailgun.net/v3",
                Authenticator = new HttpBasicAuthenticator("api", "key-3132beb8fa08a434ae0d8c3d7f022e99")
            };

            var request = new RestRequest();
            request.AddParameter("domain", "www.workmotor.com.br", ParameterType.UrlSegment);
            request.Resource = "www.workmotor.com.br/messages";
            request.AddParameter("from", "Workmotor Free <contato@workmotor.com.br>");
            request.AddParameter("to", destinatario);
            //request.AddParameter("cc", "sitefree@pequenasempresas.net");
            //request.AddParameter("cc", "felipe.negri@pequenasempresas.net");

            request.AddParameter("subject", assunto);
            request.AddParameter("html", body);
            request.Method = Method.POST;
            return client.Execute(request);
        }
    }
}