﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteWorkMotor.Helper
{
    public class TextoHelper
    {
        public string RemoverCaracteresEspeciais(string str)
        {
            const string cAcentos = "\"ÁÀÉÈÍÌÓÒÚÙÂÃÕÔÊÎÛáéíóúàèìòùãâêîôûçÁÈôÇáèÒçÂËòâëØÑÀÐøñàðÕÅõÝåÍÖýÃíöãÎÄîÚ<äÌú>ÆìÛ&æÏûïÙ®Éù©éÓÜÞÊóüþêÔº.";
            const string sAcentos = "_AAEEIIOOUUAAOOEIUaeiouaeiouaaeioucAEoCaeOcAEoaeoNADonaoOAoYAIOyAioaUAiU_aIu_EiU_eIuiUrEUceOUpEoupeO__";

            for (var i = 0; i < cAcentos.Length; i++)
                str = str.Replace(cAcentos[i].ToString(), sAcentos[i].ToString()).Trim();

            return str;
        }

        public string RemoverFormatacao(string str)
        {
            if (string.IsNullOrEmpty(str)) return string.Empty;

            return str
                .Replace("(", "")
                .Replace(")", "")
                .Replace("-", "")
                .Replace(".", "")
                .Replace("/", "");
        }

        public string RemoverFormatacao(string str, bool removerEspacoBranco)
        {
            if (string.IsNullOrEmpty(str)) return string.Empty;

            return str
                .Replace("(", "")
                .Replace(")", "")
                .Replace("-", "")
                .Replace(".", "")
                .Replace("/", "")
                .Replace(" ", "");
        }
    }
}