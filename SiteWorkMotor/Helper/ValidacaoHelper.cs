﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using RestSharp.Extensions;

namespace SiteWorkMotor.Helper
{
    public class ValidacaoHelper
    {
        public bool ValidaCpf(string vrCpf)
        {
            var valor = vrCpf.Replace(".", "");
            valor = valor.Replace("-", "");

            for (var i = valor.Length; i < 11; i++)
            {
                valor = "0" + valor;
            }

            if (valor.Length != 11)
                return false;

            var igual = true;
            for (var i = 1; i < 11 && igual; i++)
                if (valor[i] != valor[0])
                    igual = false;

            if (igual || valor == "11111111111")
                return false;
            if (valor == "22222222222")
                return false;
            if (valor == "33333333333")
                return false;
            if (valor == "44444444444")
                return false;
            if (valor == "55555555555")
                return false;
            if (valor == "66666666666")
                return false;
            if (valor == "77777777777")
                return false;
            if (valor == "88888888888")
                return false;
            if (valor == "99999999999")
                return false;
            if (valor == "00000000000")
                return false;

            if (valor == "12345678909")
                return false;

            var numeros = new int[11];
            for (var i = 0; i < 11; i++)
                numeros[i] = int.Parse(
                    valor[i].ToString());

            var soma = 0;
            for (var i = 0; i < 9; i++)
                soma += (10 - i) * numeros[i];

            var resultado = soma % 11;
            if (resultado == 1 || resultado == 0)
            {
                if (numeros[9] != 0)
                    return false;
            }
            else if (numeros[9] != 11 - resultado)
                return false;
            soma = 0;
            for (var i = 0; i < 10; i++)
                soma += (11 - i) * numeros[i];
            resultado = soma % 11;
            if (resultado == 1 || resultado == 0)
            {
                if (numeros[10] != 0)
                    return false;
            }
            else
            if (numeros[10] != 11 - resultado)
                return false;
            return true;
        }

        public bool ValidaCnpj(string vrCnpj)
        {
            var cnpj = vrCnpj.Replace(".", "");
            cnpj = cnpj.Replace("/", "");
            cnpj = cnpj.Replace("-", "");

            for (var i = cnpj.Length; i < 14; i++)
            {
                cnpj = "0" + cnpj;
            }

            const string ftmt = "6543298765432";
            var digitos = new int[14];
            var soma = new int[2];
            soma[0] = 0;
            soma[1] = 0;
            var resultado = new int[2];
            resultado[0] = 0;
            resultado[1] = 0;
            var cnpjOk = new bool[2];
            cnpjOk[0] = false;
            cnpjOk[1] = false;
            try
            {
                if (cnpj.Equals("00000000000000"))
                {
                    return false;
                }
                int nrDig;
                for (nrDig = 0; nrDig < 14; nrDig++)
                {
                    digitos[nrDig] = int.Parse(
                        cnpj.Substring(nrDig, 1));
                    if (nrDig <= 11)
                        soma[0] += (digitos[nrDig] *
                                    int.Parse(ftmt.Substring(
                                        nrDig + 1, 1)));
                    if (nrDig <= 12)
                        soma[1] += (digitos[nrDig] *
                                    int.Parse(ftmt.Substring(
                                        nrDig, 1)));
                }
                for (nrDig = 0; nrDig < 2; nrDig++)
                {
                    resultado[nrDig] = (soma[nrDig] % 11);
                    if ((resultado[nrDig] == 0) || (
                            resultado[nrDig] == 1))
                        cnpjOk[nrDig] = (
                            digitos[12 + nrDig] == 0);
                    else
                        cnpjOk[nrDig] = (
                            digitos[12 + nrDig] == (
                                11 - resultado[nrDig]));
                }
                return (cnpjOk[0] && cnpjOk[1]);
            }
            catch
            {
                return false;
            }
        }

        public bool ValidaTelefone(string numeroTelefone)
        {
            return numeroTelefone.Matches(".((10)|([1-9][1-9]).)\\s9?[6-9][0-9]{3}-[0-9]{4}") ||
                   numeroTelefone.Matches(".((10)|([1-9][1-9]).)\\s[2-5][0-9]{3}-[0-9]{4}");
        }

        public bool ValidaCep(string cep)
        {
            Regex Rgx = new Regex(@"^\d{5}-\d{3}$");

            return Rgx.IsMatch(cep);
        }
    }
}