﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SiteWorkMotor.Helper
{
    public static class PostFileHelper
    {
        public static string RecuperarTextoPost(string caminho)
        {
            if (!File.Exists(caminho)) return "";
            var streamReader = new StreamReader(caminho);
            var text = streamReader.ReadToEnd();
            streamReader.Close();
            return text;
        }

        public static string CriarCaminhoArquivo(string texto, int id)
        {
            //var caminho = @"C:\projects\workmotor\SiteWorkMotor\Content\Posts\post_" + id + ".html";
            var caminho = @"C:\IIS\WorkMotor\Content\Posts\post_" + id + ".html";
            if (!File.Exists(caminho)) File.Create(caminho).Close(); 

            File.WriteAllText(caminho,texto);

            return caminho;
        }
    }
}