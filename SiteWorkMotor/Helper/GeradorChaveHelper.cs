﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace SiteWorkMotor.Helper
{
    public class GeradorChaveHelper
    {
        public string GetUniqueKeyRNG()
        {
            RNGCryptoServiceProvider rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            byte[] randomBytes = new byte[5];
            rngCryptoServiceProvider.GetBytes(randomBytes);
            return Convert.ToBase64String(randomBytes);
        }

        public string GetUniqueKeyGuid()
        {
            return Guid.NewGuid().ToString().GetHashCode().ToString("x");
        }
    }
}