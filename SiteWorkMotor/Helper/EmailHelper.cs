﻿using System;
using System.Collections.Generic;
using Core;

namespace SiteWorkMotor.Helper
{
    public class EmailHelper
    {
        public bool EnviarEmailContato(Contato contato)
        {
            //Define os dados do e-mail

            var assunto = "Contato site Workmotor";

            var conteudo = "Contato site Workmotor <br><br> " +
                           "<br /><b>Nome</b>:" + contato.Nome +
                           "<br /><b>E-mail</b>:" + contato.Email +
                           "<br /><b>Telefone</b>:" + contato.Telefone;
            conteudo += contato.Mensagem != "Contato via home, sem opção de inserir mensagem"
                ? "Enviou a seguinte mensagem: <br><br>" + contato.Mensagem
                : ""+ ".";
            var destino =  new List<string>() { "comercial@workmotor.com.br"};
            if (contato.Assunto == null)
            {
                contato.Assunto = "Contato Workmotor";
                destino.Add("adilsongimenes@gmail.com");
            }
            else destino.Add("franquias@workmotor.com.br");
            var email = new Email(destino, "historico@workmotor.com", "Workmotor", conteudo, assunto);
            return email.SendEmail();
        }
    }
}