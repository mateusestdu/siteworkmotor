﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.Core;
using Newtonsoft.Json;
using RestSharp;
using SiteWorkMotor.Helper;
using SiteWorkMotor.ViewModel;

namespace SiteWorkMotor.Controllers
{
    public class DPKController : Controller
    {
        // GET: DPK
        private readonly PatrocinadorCore _patrocinadorCore = new PatrocinadorCore();
        private readonly TextoHelper _textoHelpers = new TextoHelper();
        private readonly ValidacaoHelper _validacaoHelpers = new ValidacaoHelper();
        private readonly EmailFreeHelper _emailHelper = new EmailFreeHelper();
        private readonly ClienteCore _clienteCore = new ClienteCore();
        private readonly LogContatoCore _logContatoCore = new LogContatoCore();
        private readonly ExeCore _exeCore = new ExeCore();
        private readonly SolicitacaoCore _solicitacaoCore = new SolicitacaoCore();

        public ActionResult Index(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                ViewBag.todos = true;
                ViewBag.tipoAcesso = id;
                return View(new ClienteViewModel());
            }

            ViewBag.todos = false;
            ViewBag.tipoAcesso = id;
            return View(new ClienteViewModel());
        }

        [HttpPost]
        public ActionResult RegistrarCliente1(ClienteViewModel modelCliente)
        {
            var cliente = new Cliente
            {
                Email = modelCliente.Email,
                Telefone = _textoHelpers.RemoverFormatacao(modelCliente.Telefone, true),
                DataCadastro = DateTime.Now,
                CpfCnpj = "",
                NomeRazaoSocial = ""
            };
            var inseriuOk = _clienteCore.Inserir(cliente);
            if (inseriuOk)
            {
                var indicacao = new Indicacao()
                {
                    RazaoSocialNome = cliente.NomeRazaoSocial,
                    Email = cliente.Email,
                    Telefone = cliente.Telefone,
                    Data = cliente.DataCadastro,
                    StatusId = 1,
                    RepresentanteId = 10,
                    PatrocinadorId = 1
                };
                if (!_clienteCore.InserirIndicacao(indicacao)) return Content("nope");
                ModelState.Clear();
                modelCliente.Id = cliente.Id;
                modelCliente.IndicacaoId = indicacao.Id;
                modelCliente.PatrocinadorId = 1;
                _emailHelper.EnviarEmailPriCadastro(cliente);
                return PartialView("_CadastroDowload", modelCliente);
            }
            return Content("nope");
        }
        [HttpPost]
        public bool RegistrarCliente(ClienteViewModel modelCliente)
        {
            modelCliente.CpfCnpj = _textoHelpers.RemoverFormatacao(modelCliente.CpfCnpj);
            modelCliente.Telefone = _textoHelpers.RemoverFormatacao(modelCliente.Telefone, true);

            modelCliente.Cep = _textoHelpers.RemoverFormatacao(modelCliente.Cep, true);

            var cliente = new Cliente();

            var url = "http://api.postmon.com.br/v1/cep/" + modelCliente.Cep;

            try
            {
                var content = GetJson(url);
                var enderecoCliente = JsonConvert.DeserializeObject<HomeController.Postmon>(content);

                cliente.Rua = enderecoCliente.logradouro;
                cliente.Bairro = enderecoCliente.bairro;
                cliente.Cidade = enderecoCliente.cidade;
                cliente.Estado = enderecoCliente.estado;
            }
            catch (Exception e)
            {
                //ignore
            }

            cliente.CpfCnpj = modelCliente.CpfCnpj;
            cliente.NomeRazaoSocial = modelCliente.RazaoSocial;
            cliente.Email = modelCliente.Email;
            cliente.Telefone = modelCliente.Telefone;
            cliente.PatrocinadorId = modelCliente.PatrocinadorId;
            cliente.DataCadastro = DateTime.Now;
            cliente.Cep = modelCliente.Cep;
            cliente.Antigo = false;
            cliente.Upgrade = false;
            cliente.Id = modelCliente.Id;

            var indicacao = _clienteCore.IndicacaoPorId(modelCliente.IndicacaoId);
            indicacao.RazaoSocialNome = cliente.NomeRazaoSocial;
            indicacao.Email = cliente.Email;
            indicacao.Telefone = cliente.Telefone;
            indicacao.Data = cliente.DataCadastro ?? new DateTime();
            indicacao.StatusId = 1;
            indicacao.Cidade = cliente.Cidade;
            indicacao.Estado = cliente.Estado;
            indicacao.RepresentanteId = 10;
            _clienteCore.AtualizaIndicacao(indicacao);

            var resp = NotificarApi(cliente);

            if (resp)
            {
                var inseriuOk = _clienteCore.Atualiza(cliente);
                if (inseriuOk)
                {
                    var patrocinador = _patrocinadorCore.RetornarPorId(cliente.PatrocinadorId);
                    return true;
                }
            }
            _emailHelper.EnviarEmailSegCadastro(cliente, _patrocinadorCore.RetornarPorId(4));
            return false;
        }

        public FileResult BaixarArquivo(int idPatrocinador)
        {
            var exe = _exeCore.RetornarExePorIdPatrocinador(idPatrocinador);
            return File(exe.Link, "application/exe", exe.Nome);
        }

        public FileResult BaixarAtualizador(string descPatrocinador)
        {
            var idPatrocinador = _patrocinadorCore.RetornarPorDescricao(descPatrocinador).Id;
            var exe = _exeCore.RetornarAtualizadorPorIdPatrocinador(idPatrocinador);
            return File(exe.Link, "application/exe", exe.Nome);
        }

        public bool NotificarApi(Cliente cli)
        {
            var personString = "<FREE><CNPJ>" + cli.CpfCnpj + "</CNPJ><RZSOCIAL>" + cli.NomeRazaoSocial +
                               "</RZSOCIAL><TELEFONE>" + cli.Telefone + "</TELEFONE><EMAIL>" + cli.Email +
                               "</EMAIL><BAIRRO>" + cli.Bairro + "</BAIRRO><ENDERECO>" + cli.Rua +
                               "</ENDERECO><CIDADE>" + cli.Cidade + "</CIDADE><ESTADO>" + cli.Estado + "</ESTADO></FREE>";

            var restClient = new RestClient("http://201.77.177.42/QueridoCarro/aFreeGravaOficina.aspx");

            var restRequest = new RestRequest("Post", Method.POST) { RequestFormat = DataFormat.Xml };

            restRequest.AddParameter("application/xml", personString, ParameterType.RequestBody);
            var response = restClient.Execute(restRequest);

            return response.StatusCode.Equals(HttpStatusCode.OK);
        }

        public ActionResult Patrocinadores(string msgRetorno)
        {
            if (string.IsNullOrEmpty(msgRetorno))
            {
                ViewBag.todos = true;
                return View();
            }
            else
            {
                if (msgRetorno.Equals("error"))
                {
                    ViewBag.EmailContatoErro = "Erro no envio de contato.";
                }
                else if (msgRetorno.Equals("sucesso"))
                {
                    ViewBag.EmailContatoSucesso1 = "Contato enviado com sucesso !";
                    ViewBag.EmailContatoSucesso2 = "Em breve entraremos em contato com você.";
                }

                ViewBag.todos = true;
                return View();
            }
        }

        [HttpPost]
        public ActionResult ContatoPatrocinador(ContatoPatrocinadorViewModel contato)
        {
            var cont = new LogContatoPatrocinador()
            {
                NomeRazaoSocial = contato.NomeRazaoSocial,
                Email = contato.Email,
                Telefone = contato.Telefone,
                Mensagem = contato.Mensagem,
                Data = DateTime.Now
            };

            _logContatoCore.Inserir(cont);

            var retorno = _emailHelper.EnviarEmailContato(cont) ? "sucesso" : "erro";

            return RedirectToAction("Index", "Parceiros", new { msgRetorno = retorno });
        }

        public ActionResult Suporte(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                ViewBag.Atualizador = false;
                ViewBag.todos = true;
                ViewBag.tipoAcesso = id;
                return View();
            }

            var pat = _patrocinadorCore.RetornarPorDescricao(id);
            var atualizador = _exeCore.RetornarAtualizadorPorIdPatrocinador(pat.Id);

            if (atualizador == null)
            {
                ViewBag.Atualizador = false;
                ViewBag.todos = false;
                ViewBag.tipoAcesso = id;
                return View();
            }

            ViewBag.DataAtualizador = atualizador.Data;
            ViewBag.Atualizador = true;
            ViewBag.todos = false;
            ViewBag.tipoAcesso = id;
            return View();
        }

        public ActionResult Upgrade(string id)
        {
            var cliente = _clienteCore.RetornarPorCpfCnpj(id);

            if (cliente != null)
            {
                var pat = _patrocinadorCore.RetornarPorId(cliente.PatrocinadorId);

                var sol = new Solicitacao()
                {
                    ClienteId = cliente.Id,
                    TipoId = 1,
                    Data = DateTime.Now,
                    StatusId = 1
                };
                _solicitacaoCore.Inserir(sol);

                _emailHelper.EnviarEmailUpgrade(cliente, pat);

                ViewBag.todos = true;
                ViewBag.Registrar = false;
            }
            else
            {
                ViewBag.Patrocinadores = new SelectList(_patrocinadorCore.RetornarTodos(), "Id", "NomeFantasia");
                ViewBag.todos = true;
                ViewBag.Registrar = true;
                var model = new ClienteViewModel
                {
                    CpfCnpj = id
                };
                return View(model);
            }

            return View();
        }

        [HttpPost]
        public bool RegistrarClienteUpgrade(ClienteViewModel modelCliente)
        {
            modelCliente.CpfCnpj = _textoHelpers.RemoverFormatacao(modelCliente.CpfCnpj);
            modelCliente.Telefone = _textoHelpers.RemoverFormatacao(modelCliente.Telefone, true);

            modelCliente.Cep = _textoHelpers.RemoverFormatacao(modelCliente.Cep, true);

            var cliente = new Cliente();

            var url = "http://api.postmon.com.br/v1/cep/" + modelCliente.Cep;

            try
            {
                var content = GetJson(url);
                var enderecoCliente = JsonConvert.DeserializeObject<HomeController.Postmon>(content);

                cliente.Rua = enderecoCliente.logradouro;
                cliente.Bairro = enderecoCliente.bairro;
                cliente.Cidade = enderecoCliente.cidade;
                cliente.Estado = enderecoCliente.estado;
            }
            catch (Exception e)
            {
                //ignore
            }

            cliente.CpfCnpj = modelCliente.CpfCnpj;
            cliente.NomeRazaoSocial = modelCliente.RazaoSocial;
            cliente.Email = modelCliente.Email;
            cliente.Telefone = modelCliente.Telefone;
            cliente.PatrocinadorId = modelCliente.PatrocinadorId;
            cliente.DataCadastro = DateTime.Now;
            cliente.Cep = modelCliente.Cep;
            cliente.Antigo = true;
            cliente.Upgrade = false;

            try
            {
                _clienteCore.Inserir(cliente);
                var patrocinador = _patrocinadorCore.RetornarPorId(cliente.PatrocinadorId);

                var solicicacao = new Solicitacao()
                {
                    ClienteId = cliente.Id,
                    TipoId = 1,
                    Data = DateTime.Now,
                    StatusId = 1
                };
                _solicitacaoCore.Inserir(solicicacao);

                _emailHelper.EnviarEmailUpgrade(cliente, patrocinador);
                return true;
            }
            catch
            {
                return false;
            }
        }

        #region .: Helpers validação :.

        [AllowAnonymous]
        [HttpPost]
        public JsonResult CpfCnpjValido(string cpfCnpj)
        {
            cpfCnpj = _textoHelpers.RemoverFormatacao(cpfCnpj);
            return Json(_validacaoHelpers.ValidaCpf(cpfCnpj) || _validacaoHelpers.ValidaCnpj(cpfCnpj));
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult TelefoneValido(string telefone)
        {
            return Json(_validacaoHelpers.ValidaTelefone(telefone));
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult CepValido(string cep)
        {
            return Json(_validacaoHelpers.ValidaCep(cep));
        }

        #endregion

        #region .: Helpers endereço :.

        protected string GetJson(string fileName)
        {
            string sData;
            try
            {
                if (fileName.ToLower().IndexOf("http:") > -1)
                {
                    var wc = new System.Net.WebClient();
                    var response = wc.DownloadData(fileName);
                    sData = System.Text.Encoding.ASCII.GetString(response);
                }
                else
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(fileName);
                    sData = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception e) { sData = e.Message; }
            return sData;
        }

        public void AtualizarEndereco()
        {
            var clientes = _clienteCore.RetornarTodos();

            foreach (var cli in clientes)
            {
                cli.Cep = _textoHelpers.RemoverFormatacao(cli.Cep, true);

                var url = "http://api.postmon.com.br/v1/cep/" + cli.Cep;

                try
                {
                    var content = GetJson(url);
                    var enderecoCliente = JsonConvert.DeserializeObject<HomeController.Postmon>(content);

                    cli.Rua = enderecoCliente.logradouro;
                    cli.Bairro = enderecoCliente.bairro;
                    cli.Cidade = enderecoCliente.cidade;
                    cli.Estado = enderecoCliente.estado;
                    var resp = _clienteCore.Atualiza(cli);
                }
                catch (Exception e)
                {
                    //ignore
                }
            }
        }

        public struct Postmon
        {
            public string complemento;
            public string bairro;
            public string cidade;
            public string logradouro;
            public HomeController.Estado_info estado_info;
            public string cep;
            public HomeController.Cidade_info cidade_info;
            public string estado;
        }

        public struct Estado_info
        {
            public string area_km2;
            public string codigo_ibge;
            public string nome;
        }

        public struct Cidade_info
        {
            public string area_km2;
            public string codigo_ibge;
        }

        #endregion

    }
}
