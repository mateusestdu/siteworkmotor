﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.Core;
using Core.Security;
using Domain.Security;
using SiteWorkMotor.ViewModel;

namespace SiteWorkMotor.Controllers

{
    public class AdministrativoController : Controller
    {
        private readonly UsuarioCore _usuarioCore = new UsuarioCore();
        private readonly ContatoCore _contato = new ContatoCore();
        private readonly FormsAuthenticationService _authentication = new FormsAuthenticationService(System.Web.HttpContext.Current);
        // GET: Administrativo
        public ActionResult Index()
        {
            if (new UsuarioAutenticado().Usuario != null)
            {
                var contatos = _contato.TodosContatos();
                _contato.AtualizarContatosVisualizados(contatos);
                return View("Contato", contatos);

            }
            return View();
        }
        [HttpPost]
        public ActionResult Logar(UsuarioViewModel usuario)
        {
            if (!ModelState.IsValid)
            {
                return View("Index", usuario);
            }
            if (_usuarioCore.Autenticar(usuario.Login, usuario.Senha, 1))
            {
                var u = _usuarioCore.PorLogin(usuario.Login);
                _authentication.SignIn(u, false);
                return View("Contato", _contato.TodosContatos());
            }

            @ViewBag.mensagem = "Usuário não autenticado.";
            return View("Index", usuario);

        }
        public ActionResult Logout()
        {
            Session.Abandon();
            _authentication.SignOut();
            return RedirectToAction("Index");
        }

    }


}