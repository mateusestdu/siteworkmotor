﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.Core;
using Newtonsoft.Json;
using SiteWorkMotor.Helper;
using SiteWorkMotor.ViewModel;

namespace SiteWorkMotor.Controllers
{
    public class FreeController : Controller
    {
        // GET: Free
        private readonly ContatoCore _contatoCore = new ContatoCore();
        private readonly PostCore _postCore = new PostCore();
        private readonly EmailHelper _emailHelper = new EmailHelper();
        private readonly PatrocinadorCore _patrocinadorCore = new PatrocinadorCore();
        private readonly TextoHelper _textoHelpers = new TextoHelper();
        private readonly ValidacaoHelper _validacaoHelpers = new ValidacaoHelper();
        private readonly EmailFreeHelper _emailFreeHelper = new EmailFreeHelper(); //workmotor Free
        private readonly SMSHelper _smsHelper = new SMSHelper(); //ideia tirada do CRM
        private readonly GeradorChaveHelper _geradorChaveHelper = new GeradorChaveHelper();
        private readonly ClienteCore _clienteCore = new ClienteCore();
        private readonly LogContatoCore _logContatoCore = new LogContatoCore();
        private readonly ExeCore _exeCore = new ExeCore();
        private readonly SolicitacaoCore _solicitacaoCore = new SolicitacaoCore();
        private readonly SmsHistoricoCore _smsHistoricoCore = new SmsHistoricoCore();


        public ActionResult Index(string id)
        {

            ViewBag.Blog = _postCore.MaisNovos(3).Select(post => new PublicacaoViewModel()
            {
                PalavrasChave = post.PALAVRASCHAVESPOSTS.Select(x => x.Descricao).ToList(),
                TituloTexto = post.Titulo,
                PublicadoEm = post.DataPublicacao ?? post.DataCriacao ?? new DateTime(),
                Resumo = post.Resumo,
                TextoPublicacao = post.PathNoticia,
                Id = post.Id,
                Url = PostHelper.UrlPorPalavraChave(post.PALAVRASCHAVESPOSTS.Select(x => x.Descricao).ToList())
            }).ToList();

            if (string.IsNullOrEmpty(id))
            {
                //ViewBag.Patrocinadores = new SelectList(_patrocinadorCore.RetornarTodos(), "Id", "NomeFantasia");
                ViewBag.todos = true;
                ViewBag.tipoAcesso = id;
                ViewBag.Device = CheckDevice();
                return View(new ClienteViewModel());
            }

            ViewBag.todos = false;
            ViewBag.tipoAcesso = id;
            ViewBag.Device = CheckDevice();

            return View(new ClienteViewModel());
        }

        [HttpPost]
        public ActionResult RegistrarCliente1(ClienteViewModel modelCliente)
        {

            modelCliente.Telefone = _textoHelpers.RemoverFormatacao(modelCliente.Telefone, true);

            var cliente = new Cliente
            {
                Email = modelCliente.Email,
                Telefone = modelCliente.Telefone,
                DataCadastro = DateTime.Now,
                CpfCnpj = "",
                NomeRazaoSocial = ""
            };

            var inseriuOk = _clienteCore.Inserir(cliente);

            if (inseriuOk)
            {
                var chaveGuid = _geradorChaveHelper.GetUniqueKeyGuid();

                _smsHelper.EnviarSmsPersonalizado(cliente, chaveGuid);

                ViewBag.Patrocinadores = new SelectList(_patrocinadorCore.RetornarTodos(), "Id", "NomeFantasia");
                ModelState.Clear();
                modelCliente.Id = cliente.Id;
                _emailFreeHelper.EnviarEmailPriCadastro(cliente);
                ViewBag.Device = CheckDevice();
                var smsHistorico = _smsHistoricoCore.ObterPorClienteTelefone(cliente.Id, cliente.Telefone);
                if (smsHistorico == null) return Content("Fail");
                ViewBag.SmsHistoricoAtivo = smsHistorico.DataAtivacao;
                ViewBag.SmsHistoricoId = smsHistorico.Id;
                return PartialView("_CadastroDownload", modelCliente);
            }
            return Content("Fail");
        }

        [HttpPost]
        public bool RegistrarCliente(ClienteViewModel modelCliente)
        {
            modelCliente.CpfCnpj = _textoHelpers.RemoverFormatacao(modelCliente.CpfCnpj);
            modelCliente.Telefone = _textoHelpers.RemoverFormatacao(modelCliente.Telefone, true);

            modelCliente.Cep = _textoHelpers.RemoverFormatacao(modelCliente.Cep, true);

            var cliente = new Cliente();

            var url = "http://api.postmon.com.br/v1/cep/" + modelCliente.Cep;

            try
            {
                var content = GetJson(url);
                var enderecoCliente = JsonConvert.DeserializeObject<Postmon>(content);

                cliente.Rua = enderecoCliente.logradouro;
                cliente.Bairro = enderecoCliente.bairro;
                cliente.Cidade = enderecoCliente.cidade;
                cliente.Estado = enderecoCliente.estado;
            }
            catch (Exception e)
            {
                //ignore
            }

            cliente.CpfCnpj = modelCliente.CpfCnpj;
            cliente.NomeRazaoSocial = modelCliente.RazaoSocial;
            cliente.Email = modelCliente.Email;
            cliente.Telefone = modelCliente.Telefone;
            cliente.PatrocinadorId = modelCliente.PatrocinadorId;
            cliente.DataCadastro = DateTime.Now;
            cliente.Cep = modelCliente.Cep;
            cliente.Antigo = false;
            cliente.Upgrade = false;
            cliente.Id = modelCliente.Id;

            var indicacao = new Indicacao()
            {
                RazaoSocialNome = cliente.NomeRazaoSocial,
                Email = cliente.Email,
                Telefone = cliente.Telefone,
                Data = cliente.DataCadastro,
                StatusId = 1,
                RepresentanteId = 10,
                Cidade = cliente.Cidade,
                Estado = cliente.Estado,
                PatrocinadorId = cliente.PatrocinadorId
            };
            if (!_clienteCore.InserirIndicacao(indicacao)) return false;
            modelCliente.IndicacaoId = indicacao.Id;

            var inseriuOk = _clienteCore.Atualiza(cliente);
            if (inseriuOk)
            {
                var patrocinador = _patrocinadorCore.RetornarPorId(cliente.PatrocinadorId);
            }
            _emailFreeHelper.EnviarEmailSegCadastro(cliente, _patrocinadorCore.RetornarPorId(modelCliente.PatrocinadorId));
            return true;
        }

        public ActionResult ValidacaoChave(int clienteId, int smsHistoricoId, string chave)
        {
            try
            {
                var historico = _smsHistoricoCore.ObterPorId(smsHistoricoId);
                TimeSpan ts = DateTime.Now.Subtract(historico.DataEnvio);

                if (ts.TotalMinutes < 20 && historico.ChaveSeguranca.Trim() == chave.Trim())
                {
                    historico.DataAtivacao = DateTime.Now;
                    if(!_smsHistoricoCore.Atualizar(historico)) return Content("Fail");
                    ViewBag.SmsHistoricoAtivo = historico.DataAtivacao;
                    var cliente = _clienteCore.RetornarPorId(clienteId);
                    ViewBag.Patrocinadores = new SelectList(_patrocinadorCore.RetornarTodos(), "Id", "NomeFantasia");
                    var viewModelCliente = new ClienteViewModel
                    {
                        Id = cliente.Id,
                        CpfCnpj = cliente.CpfCnpj,
                        RazaoSocial = cliente.NomeRazaoSocial,
                        Email = cliente.Email,
                        Telefone = cliente.Telefone,
                        Cep = cliente.Cep,
                        PatrocinadorId = cliente.PatrocinadorId??0
                    };
                    return PartialView("_CadastroDownload", viewModelCliente);
                }
                return Content("Fail");
            }
            catch (Exception e)
            {
                return Content("Fail");
            }
        }

        public bool EnviarEmailDownload(Cliente cliente)
        {
            try
            {
                var cli = _clienteCore.RetornarPorId(cliente.Id);
                _emailFreeHelper.EnviarEmailDownload(cli);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public FileResult BaixarArquivo(int idPatrocinador)
        {
            var exe = _exeCore.RetornarExePorIdPatrocinador(idPatrocinador);
            return File(exe.Link, "application/exe", exe.Nome);
        }

        public FileResult BaixarAtualizador(string descPatrocinador)
        {
            var idPatrocinador = _patrocinadorCore.RetornarPorDescricao(descPatrocinador).Id;
            var exe = _exeCore.RetornarAtualizadorPorIdPatrocinador(idPatrocinador);
            return File(exe.Link, "application/exe", exe.Nome);
        }

        [HttpPost]
        public bool RegistrarClienteUpgrade(ClienteViewModel modelCliente)
        {
            modelCliente.CpfCnpj = _textoHelpers.RemoverFormatacao(modelCliente.CpfCnpj);
            modelCliente.Telefone = _textoHelpers.RemoverFormatacao(modelCliente.Telefone, true);

            modelCliente.Cep = _textoHelpers.RemoverFormatacao(modelCliente.Cep, true);

            var cliente = new Cliente();

            var url = "http://api.postmon.com.br/v1/cep/" + modelCliente.Cep;

            try
            {
                var content = GetJson(url);
                var enderecoCliente = JsonConvert.DeserializeObject<Postmon>(content);

                cliente.Rua = enderecoCliente.logradouro;
                cliente.Bairro = enderecoCliente.bairro;
                cliente.Cidade = enderecoCliente.cidade;
                cliente.Estado = enderecoCliente.estado;
            }
            catch (Exception e)
            {
                //ignore
            }

            cliente.CpfCnpj = modelCliente.CpfCnpj;
            cliente.NomeRazaoSocial = modelCliente.RazaoSocial;
            cliente.Email = modelCliente.Email;
            cliente.Telefone = modelCliente.Telefone;
            cliente.PatrocinadorId = modelCliente.PatrocinadorId;
            cliente.DataCadastro = DateTime.Now;
            cliente.Cep = modelCliente.Cep;
            cliente.Antigo = true;
            cliente.Upgrade = false;

            try
            {
                _clienteCore.Inserir(cliente);
                var patrocinador = _patrocinadorCore.RetornarPorId(cliente.PatrocinadorId);

                var solicicacao = new Solicitacao()
                {
                    ClienteId = cliente.Id,
                    TipoId = 1,
                    Data = DateTime.Now,
                    StatusId = 1
                };
                _solicitacaoCore.Inserir(solicicacao);

                _emailFreeHelper.EnviarEmailUpgrade(cliente, patrocinador);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private string CheckDevice()
        {
            string u = Request.ServerVariables["HTTP_USER_AGENT"];
            Regex b = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            if ((b.IsMatch(u) || v.IsMatch(u.Substring(0, 4))))
            {
                return "Mobile";
            }
            else
            {
                return "Desktop";
            }
        }

        public string ExitIntent()
        {
            if (Session["ExitIntent"] != null)
            {
                return "Not null";
            }
            else
            {
                Session["ExitIntent"] = "Not null";
                return "Null";
            }
            
        }

        #region .: Helpers validação :.

        [AllowAnonymous]
        [HttpPost]
        public JsonResult CpfCnpjValido(string cpfCnpj)
        {
            cpfCnpj = _textoHelpers.RemoverFormatacao(cpfCnpj);
            return Json(_validacaoHelpers.ValidaCpf(cpfCnpj) || _validacaoHelpers.ValidaCnpj(cpfCnpj));
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult TelefoneValido(string telefone)
        {
            return Json(_validacaoHelpers.ValidaTelefone(telefone));
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult CepValido(string cep)
        {
            return Json(_validacaoHelpers.ValidaCep(cep));
        }

        #endregion

        #region .: Helpers endereço :.

        protected string GetJson(string fileName)
        {
            string sData;
            try
            {
                if (fileName.ToLower().IndexOf("http:") > -1)
                {
                    var wc = new System.Net.WebClient();
                    var response = wc.DownloadData(fileName);
                    sData = System.Text.Encoding.ASCII.GetString(response);
                }
                else
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(fileName);
                    sData = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception e) { sData = e.Message; }
            return sData;
        }

        public void AtualizarEndereco()
        {
            var clientes = _clienteCore.RetornarTodos();

            foreach (var cli in clientes)
            {
                cli.Cep = _textoHelpers.RemoverFormatacao(cli.Cep, true);

                var url = "http://api.postmon.com.br/v1/cep/" + cli.Cep;

                try
                {
                    var content = GetJson(url);
                    var enderecoCliente = JsonConvert.DeserializeObject<Postmon>(content);

                    cli.Rua = enderecoCliente.logradouro;
                    cli.Bairro = enderecoCliente.bairro;
                    cli.Cidade = enderecoCliente.cidade;
                    cli.Estado = enderecoCliente.estado;
                    var resp = _clienteCore.Atualiza(cli);
                }
                catch (Exception e)
                {
                    //ignore
                }
            }
        }

        public struct Postmon
        {
            public string complemento;
            public string bairro;
            public string cidade;
            public string logradouro;
            public Estado_info estado_info;
            public string cep;
            public Cidade_info cidade_info;
            public string estado;
        }

        public struct Estado_info
        {
            public string area_km2;
            public string codigo_ibge;
            public string nome;
        }

        public struct Cidade_info
        {
            public string area_km2;
            public string codigo_ibge;
        }
    }

    #endregion
}