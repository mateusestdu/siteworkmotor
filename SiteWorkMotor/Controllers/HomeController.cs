﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.Xml;
using Core;
using Core.Core;
using SiteWorkMotor.Helper;
using SiteWorkMotor.ViewModel;

namespace SiteWorkMotor.Controllers
{
    public class HomeController : Controller
    {
        private readonly ContatoCore _contatoCore = new ContatoCore();
        private readonly PostCore _postCore = new PostCore();
        private readonly EmailHelper _emailHelper =new EmailHelper();

       
        public ActionResult Index()
        {
            ViewBag.Blog = _postCore.MaisNovos(3).Select(post => new PublicacaoViewModel()
            {
                PalavrasChave = post.PALAVRASCHAVESPOSTS.Select(x => x.Descricao).ToList(),
                TituloTexto = post.Titulo,
                PublicadoEm = post.DataPublicacao ?? post.DataCriacao ?? new DateTime(),
                Resumo = post.Resumo,
                TextoPublicacao = post.PathNoticia,
                Id = post.Id,
                Url = PostHelper.UrlPorPalavraChave(post.PALAVRASCHAVESPOSTS.Select(x => x.Descricao).ToList())
            }).ToList();

            ViewBag.Device = CheckDevice();

            return View();
        }

        [HttpPost]
        public ActionResult Contato(ContatoViewModel contato)
        {
            if (!ModelState.IsValid)
                return PartialView("_Contato", contato);
            contato.Telefone = contato.Telefone.Replace("(", "").Replace(")", "").Replace("-", "");
            var c = new Contato()
            {
                Nome = contato.Nome,
                Email = contato.Email,
                Telefone = contato.Telefone,
                Mensagem = "Contato via home, sem opção de inserir mensagem",
                Tipo = 1, // tipo contato
                UrlCv = "",
                Assunto = contato.Assunto
            };
            _contatoCore.AddContato(c);
            _emailHelper.EnviarEmailContato(c);
            return Content("OK");
        }
        [HttpPost]
        public ActionResult ContatoMsg(ContatoMsgViewModel contato)
        {
            if (!ModelState.IsValid)
                return PartialView("_Contato", contato);
            contato.Telefone = contato.Telefone.Replace("(", "").Replace(")", "").Replace("-", "");
            var c = new Contato()
            {
                Nome = contato.Nome,
                Email = contato.Email,
                Telefone = contato.Telefone,
                Mensagem = contato.Mensagem,
                Tipo = 1, // tipo contato
                UrlCv = "",
                Assunto = contato.Assunto
            };
            _contatoCore.AddContato(c);
            _emailHelper.EnviarEmailContato(c);
            return Content("OK");
        }
        [HttpPost]
        public ActionResult ContatoCv(ContatoCvViewModel contato)
        {
            if (!ModelState.IsValid)
                return PartialView("_Contato", contato);
            contato.TelefoneCv = contato.TelefoneCv.Replace("(", "").Replace(")", "").Replace("-", "");
            var contatocv = new Contato()
            {
                Nome = contato.NomeCv,
                Email = contato.EmailCv,
                Telefone = contato.TelefoneCv,
                Mensagem = contato.MensagemCv,
                Tipo = 2, // tipo colaborador,
                UrlCv = ""
            };
            var c = _contatoCore.AddContato(contatocv);
            //_emailHelper.EnviarEmailContato(c);
            return Content(c.Id.ToString());
        }

        public bool SalvarCvContato(int id)
        {
            if (Request.Files.Count > 0)
            {
                foreach (var url in UploadArquivos(id, Request.Files))
                {
                    _contatoCore.SalvarCv(id, url);
                }
                return true;
            }
            return true;
        }
        public List<string> UploadArquivos(int id, HttpFileCollectionBase filesBase)
        {
            var list = new List<string>();
            if (filesBase.Count > 0)
            {
                try
                {
                    var files = filesBase;
                    var arquivos = new List<string>();
                    for (int i = 0; i < files.Count; i++)
                    {
                        var file = files[i];
                        string fname = "";

                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            if (file != null)
                            {
                                var testfiles = file.FileName.Split(new char[] { '\\' });
                                fname = testfiles[testfiles.Length - 1];
                            }
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        var path = Server.MapPath("~/Content/Cvs/Cv_" + id + "/");
                        Directory.CreateDirectory(path);
                        fname = Path.Combine(path, fname);
                        file.SaveAs(fname);
                        arquivos.Add(fname);
                        list.AddRange(arquivos);
                    }
                }
                catch (Exception ex)
                {
                    return list;
                }
            }
            return list;
        }

        private string CheckDevice()
        {
            string u = Request.ServerVariables["HTTP_USER_AGENT"];
            Regex b = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            if ((b.IsMatch(u) || v.IsMatch(u.Substring(0, 4))))
            {
                return "Mobile";
            }
            else
            {
                return "Desktop";
            }
        }

        //ADIÇÃO DR.MACETE

        public struct Postmon
        {
            public string complemento;
            public string bairro;
            public string cidade;
            public string logradouro;
            public Estado_info estado_info;
            public string cep;
            public Cidade_info cidade_info;
            public string estado;
        }

        public struct Estado_info
        {
            public string area_km2;
            public string codigo_ibge;
            public string nome;
        }

        public struct Cidade_info
        {
            public string area_km2;
            public string codigo_ibge;
        }
    }
}