﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Core.Core;
using SiteWorkMotor.Helper;
using SiteWorkMotor.ViewModel;

namespace SiteWorkMotor.Controllers
{
    public class BlogController : Controller
    {
        private readonly PostCore _postCore = new PostCore();

        [ValidateInput(false)]
        [Route("noticia/{id:int}/{descricao:string}")]
        public ActionResult Noticia(int id, string descricao)
        {
            var post = _postCore.PorId(id);
            return View("Noticia", new PublicacaoViewModel()
            {
                Id = post.Id,
                PalavrasChave = post.PALAVRASCHAVESPOSTS.Select(x => x.Descricao).ToList(),
                TextoPublicacao = PostFileHelper.RecuperarTextoPost(post.PathNoticia),
                TituloTexto = post.Titulo,
                Resumo = post.Resumo,
                PathMiniatura = post.PathMiniatura.Replace("\\", "/"),
                PublicadoEm = post.DataPublicacao ?? new DateTime()
            });
        }
        public ActionResult Noticias()
        {
            var posts = _postCore.Todos();

            foreach (var p in posts)
            {
                if (p.PathMiniatura != null)
                {
                    string path = p.PathMiniatura.Substring(p.PathMiniatura.LastIndexOf("\\Content"));
                    p.PathMiniatura = path;
                }
                
            }

            return View("Index", new PublicacoesViewModel()
            {
                Publicacao = posts.OrderByDescending(x => x.DataPublicacao).Select(PostHelper.ConverterPost).ToList(),
                PalavrasChave = PalavrasChavesMaisUsadas(posts.SelectMany(x => x.PALAVRASCHAVESPOSTS).Select(x => x.Descricao)),
            });
        }

        public ActionResult NoticiasPorPalavraChave(string palavra)
        {
            var posts = _postCore.PorPalavraChave(palavra);

            foreach (var p in posts)
            {
                if (p.PathMiniatura != null)
                {
                    string path = p.PathMiniatura.Substring(p.PathMiniatura.LastIndexOf("\\Content"));
                    p.PathMiniatura = path;
                }

            }

            return View("Index", new PublicacoesViewModel()
            {
                Publicacao = posts.OrderByDescending(x => x.DataPublicacao).Select(PostHelper.ConverterPost).ToList(),
                PalavrasChave = PalavrasChavesMaisUsadas(posts.SelectMany(x => x.PALAVRASCHAVESPOSTS).Select(x => x.Descricao))
            });
        }
        public ActionResult PesquisarNoticina(string palavra)
        {
            var posts = _postCore.PorPalavraChave(palavra);
            posts.AddRange(_postCore.ContainsDescricao(palavra));
            posts = posts.Distinct().ToList();

            foreach (var p in posts)
            {
                if (p.PathMiniatura != null)
                {
                    string path = p.PathMiniatura.Substring(p.PathMiniatura.LastIndexOf("\\Content"));
                    p.PathMiniatura = path;
                }

            }

            return View("Index", new PublicacoesViewModel()
            {
                Publicacao = posts.OrderByDescending(x => x.DataPublicacao).Select(PostHelper.ConverterPost).ToList(),
                PalavrasChave = PalavrasChavesMaisUsadas(posts.SelectMany(x => x.PALAVRASCHAVESPOSTS).Select(x => x.Descricao))
            });
        }

        private static List<string> PalavrasChavesMaisUsadas(IEnumerable<string> palavras)
        {
            return palavras.GroupBy(x => x).OrderByDescending(x => x.Count()).Take(5).Select(x => x.Key).ToList();

        }
    }
}