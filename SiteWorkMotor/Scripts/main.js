﻿function EnviarFormulario(dados, url, form) {
    $.ajax({
        type: "POST",
        url: url,
        data: dados,
        success: function (data) {
            if (data === "OK") {
                if (form !== "index") {
                    console.log("É da Index");
                    $('#modalcontato').modal("show");
                } else {
                    console.log("Não é da Index: " + form);
                }
                $("#Nome").val("");
                $("#Email").val("");
                $("#Telefone").val("");
                $("#Mensagem").val("");
            } else {
                $("#formcontato").html(data);
            }
        },
        error: function(data) {
            //alert("ajskdkaso");
        }
    });
    return false;
}
function EnviarFormularioCv(dados, url) {
    $.ajax({
        type: "POST",
        url: url,
        data: dados,
        success: function (data) {
            if (window.FormData !== undefined) {
                var fileUpload = $("#selecao-arquivo").get(0);
                var files = fileUpload.files;
                var fileData = new FormData();
                // Looping over all files and add it to FormData object
                for (var i = 0; i < files.length; i++) {
                    fileData.append(files[i].name, files[i]);
                }
                $.ajax({
                    url: "/Home/SalvarCvContato?id=" + data,
                    type: "POST",
                    contentType: false, // Not to set any content header
                    processData: false, // Not to process data
                    data: fileData,
                    success: function (result) {
                        $('#modalcontato').modal("show");
                        $("#NomeCv").val("");
                        $("#EmailCv").val("");
                        $("#TelefoneCv").val("");
                        $("#MensagemCv").val("");
                    },
                    error: function (err) {
                    }
                });
            }
        }
    });
    return false;
}

workFree = {
    mascararContato: function () {
        $("#telefone")
                .focusin(function (event) {
                    var target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                    var phone = target.value.replace(/\D/g, '');
                    var element = $(target);
                    element.unmask();
                    element.mask("(99) 9999999999");
                }),

            $("#telefone")
                .focusout(function (event) {
                    var target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                    var phone = target.value.replace(/\D/g, '');
                    var element = $(target);
                    element.unmask();
                    if (phone.length > 10) {
                        element.mask("(99) 99999-9999");
                    } else {
                        element.mask("(99) 9999-9999");
                    }
                });
    },

    mascararCpfCnpj: function () {
        $("#cpfCnpj")
                .focusin(function (event) {
                    var target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                    var phone = target.value.replace(/\D/g, "");
                    var element = $(target);
                    element.unmask();
                    element.mask("99999999999999");
                }),

            $("#cpfCnpj")
                .focusout(function (event) {
                    var target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                    var phone = target.value.replace(/\D/g, "");
                    var element = $(target);
                    element.unmask();
                    if (phone.length > 11) {
                        element.mask("99.999.999/9999-99");
                    } else {
                        element.mask("999.999.999-99");
                    }
                });
    },

    mascararEndereco: function () {
        $("#cep")
                .focusin(function (event) {
                    var target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                    var phone = target.value.replace(/\D/g, "");
                    var element = $(target);
                    element.unmask();
                    element.mask("99999-999");
                }),

            $("#cep")
                .focusout(function (event) {
                    var target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                    var phone = target.value.replace(/\D/g, "");
                    var element = $(target);
                    element.unmask();
                    element.mask("99999-999");
                });
    }

};
