function Util() {

	Util.prototype.Inicializar = function Inicializar() {
		window.util.InicializaFullHeight();
		window.util.ScrollPage();
	}


	// FULL HEIGHT
	Util.prototype.InicializaFullHeight = function InicializaFullHeight() {

		if (jQuery(window).width() > 768) {
			var windowH = $(window).height();
			var wrapperH = $('.fullHeight').height();
			if (windowH > wrapperH) {
				$('.fullHeight').css({ 'height': ($(window).height()) + 'px' });
			}
			$(document).ready(function () {
				var windowH = $(window).height();
				var wrapperH = $('.fullHeight').height();
				var differenceH = windowH - wrapperH;
				var newH = wrapperH + differenceH;
				var truecontentH = $('#truecontent').height();
				if (windowH > truecontentH) {
					$('.fullHeight').css('height', (newH) + 'px');
				}

			})
		}

	}

	Util.prototype.ScrollPage = function ScrollPage() {
		(function($){
			$(window).on("load",function(){
				
				/* Page Scroll to id fn call */
				$("#menu a,a[href='#top'],a[rel='id']").mPageScroll2id({
					highlightSelector:"#menu a",
				});
				
				/* demo functions */
				$("a[rel='next']").click(function(e){
					e.preventDefault();
					var to=$(this).parent().parent("section").next().attr("id");
					$.mPageScroll2id("scrollTo",to);
				});
				
			});
		})(jQuery);

	}


}