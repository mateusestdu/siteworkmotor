﻿using System.Web.Optimization;

namespace SiteWorkMotor
{
    public class BundleConfig
    {
        // Para obter mais informações sobre o agrupamento, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery.validate.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/landingpage").Include(
                "~/Scripts/util.js",
                "~/Scripts/jquery.sidr.min.js",
                "~/Scripts/jquery.malihu.PageScroll2id.min.js",
                "~/Scripts/jquery.malihu.PageScroll2id.min(1).js",
                "~/Scripts/jquery.mask.js",
                "~/Scripts/jquery.magnific-popup.min.js",
                "~/Scripts/main.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/editorJs").Include(
                "~/Scripts/bootstrap-tagsinput.min.js"
            ));

            // Use a versão em desenvolvimento do Modernizr para desenvolver e aprender. Em seguida, quando estiver
            // pronto para a produção, utilize a ferramenta de build em https://modernizr.com para escolher somente os testes que precisa.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/jquery.dataTables.min.js",
                "~/Scripts/respond.js"));
            
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/site.css",
                "~/Content/jquery.dataTables.min.css",
                "~/Content/dataTable.bootstrap4.min.css",
                "~/Content/magnific-popup.css",
                "~/Content/font-awesome.min.css"
               ));
                
            bundles.Add(new StyleBundle("~/Content/editorCss").Include(
                "~/Content/bootstrap-tagsinput.css"
               ));

            bundles.Add(new StyleBundle("~/Content/cssmainDrMacete").Include(
                "~/Content/bootstrap.css",
                "~/Content/font-awesome.min.css",
                "~/Content/normalize.css",
                "~/Content/foundation.min.css",
                "~/Content/style.css",
                "~/Content/jquery.circular-carousel.css",
                "~/Content/mainDrMacete.css"));

            bundles.Add(new StyleBundle("~/Content/cssmainDPK").Include(
                "~/Content/bootstrap.css",
                "~/Content/font-awesome.min.css",
                "~/Content/normalize.css",
                "~/Content/foundation.min.css",
                "~/Content/style.css",
                "~/Content/jquery.circular-carousel.css",
                "~/Content/mainDPK.css"));

            bundles.Add(new StyleBundle("~/Content/cssmainMotoPecas").Include(
                "~/Content/bootstrap.css",
                "~/Content/font-awesome.min.css",
                "~/Content/normalize.css",
                "~/Content/foundation.min.css",
                "~/Content/style.css",
                "~/Content/jquery.circular-carousel.css",
                "~/Content/mainMotoPecas.css"));

            bundles.Add(new StyleBundle("~/Content/cssmainTSPneus").Include(
                "~/Content/bootstrap.css",
                "~/Content/font-awesome.min.css",
                "~/Content/normalize.css",
                "~/Content/foundation.min.css",
                "~/Content/style.css",
                "~/Content/jquery.circular-carousel.css",
                "~/Content/mainTSPneus.css"));

        }
    }
}
