﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SiteWorkMotor
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.RouteExistingFiles = true;

            //routes.MapRoute(
            //    "BlogRoute",
            //    "blog/{action}/{id}",
            //    new { controller = "blog" },
            //    namespaces: new[] { "SiteWorkMotor.Areas.Conteudo.Controllers" }
            //);

            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}/{descricao}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional, descricao = UrlParameter.Optional },
                namespaces: new[] { "SiteWorkMotor.Areas.Conteudo.Controllers" }
            );
        }
    }
}

