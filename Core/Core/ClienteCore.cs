﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Security;

namespace Core.Core
{
    public class ClienteCore
    {
        private readonly BdSponsorEntities _db = new BdSponsorEntities();

        public Cliente RetornarPorId(int id)
        {
            return _db.CLIENTES.FirstOrDefault(c => c.Id == id);
        }

        public bool ValidarCliente(int id, string cod)
        {
            var cli = _db.CLIENTES.FirstOrDefault(c => c.Id == id);
            if (cli != null && Encryptor.Md5Hash(cli.Email).Equals(cod)) return true;
            return false;
        }

        public bool ConfirmarCliente(int id)
        {
            var cli = _db.CLIENTES.FirstOrDefault(u => u.Id == id);
            if (cli == null) return false;
            _db.CLIENTES.Attach(cli);
            _db.Entry(cli).State = EntityState.Modified;
            _db.SaveChanges();
            return true;
        }

        public bool Inserir(Cliente cli)
        {
            try
            {
                _db.CLIENTES.Add(cli);
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                var a = e.Message;
                return false;
            }
        }

        public Cliente ExisteCliente(string cpfCnpj, int? patrocinadorId)
        {
            return _db.CLIENTES.FirstOrDefault(c => c.CpfCnpj == cpfCnpj && c.PatrocinadorId == patrocinadorId);
        }

        public List<Cliente> RetornarTodos()
        {
            return _db.CLIENTES.ToList();
        }

        public Cliente RetornarPorCpfCnpj(string cpfCnpj)
        {
            return _db.CLIENTES.FirstOrDefault(c => c.CpfCnpj == cpfCnpj);
        }

        public bool Atualiza(Cliente cli)
        {
            try
            {
                _db.CLIENTES.Attach(cli);
                _db.Entry(cli).State = EntityState.Modified;
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public Indicacao IndicacaoPorId(int id)
        {
            return _db.INDICACOES.FirstOrDefault(x => x.Id == id);
        }
        public bool AtualizaIndicacao(Indicacao cli)
        {
            try
            {
                _db.INDICACOES.Attach(cli);
                _db.Entry(cli).State = EntityState.Modified;
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool InserirIndicacao(Indicacao objeto)
        {
            try
            {
                _db.INDICACOES.Add(objeto);
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                var a = e.Message;
                return false;
            }
        }
    }
}
