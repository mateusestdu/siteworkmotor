﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Core.Core
{
    public class PostCore
    {
        private readonly BdSiteWorkmotorEntities _bd = new BdSiteWorkmotorEntities();

        public List<Post> TodosSemStatus()
        {
            return _bd.POSTS.Where(x=> x.StatusId != 4).ToList();
        }
        
        public List<Post> Todos()
        {
            return _bd.POSTS.Where(x => x.StatusId == 2 && x.StatusId != 4).ToList();
        }

        public Post PorId(int id)
        {
            return _bd.POSTS.FirstOrDefault(x => x.Id == id);
        }

        public Post Salvar(Post post)
        {
            _bd.POSTS.Add(post);
            _bd.Entry(post).State  = EntityState.Added;
            _bd.SaveChanges();
            return post;
        }

        public Post SalvarCaminho(int postId, string caminho)
        {
            var p = PorId(postId);
            p.PathNoticia = caminho;
            _bd.Entry(p).State  = EntityState.Modified;
            _bd.SaveChanges();
            return p;
        }

        public Post Atualizar(Post post)
        {
            _bd.Entry(post).State = EntityState.Modified;
            _bd.SaveChanges();
            return post;
        }

        public List<Post> PorPalavraChave(string palavra)
        {
            return _bd.PALAVRASCHAVESPOSTS.Where(x => x.Habilitado == true && x.POSTS.StatusId == 2 && x.Descricao.Contains(palavra)).Select(x => x.POSTS).Distinct().ToList();
        }

        public IEnumerable<Post> ContainsDescricao(string palavra)
        {
            return _bd.POSTS.Where(x => x.StatusId == 2 && x.Resumo.Contains(palavra)).Distinct();
        }

        public IEnumerable<Post> MaisNovos(int qtd)
        {
            return _bd.POSTS.Where(x => x.StatusId == 2).OrderByDescending(x=>x.DataPublicacao).Take(qtd);
        }
    }
}
