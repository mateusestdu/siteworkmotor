﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Core
{
    public class SolicitacaoCore
    {
        private readonly BdSponsorEntities _db = new BdSponsorEntities();

        public bool Inserir(Solicitacao sol)
        {
            try
            {
                _db.SOLICITACOES.Add(sol);
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                var a = e.Message;
                return false;
            }
        }
    }
}
