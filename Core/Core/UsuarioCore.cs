﻿using System.Linq;

namespace Core.Core
{
    public class UsuarioCore
    {
        private readonly BdSiteWorkmotorEntities _bdSiteWorkmotor = new BdSiteWorkmotorEntities();

        public bool Autenticar(string login, string senha,int tipo)
        {
            var u = _bdSiteWorkmotor.USUARIOS.FirstOrDefault(x => x.Email == login && x.TipoUsuarioId == tipo);
            if (u == null)
            {
                return false;
            }
            return (u.Senha == senha);
        }
        public Usuario PorId(int id)
        {
            return (_bdSiteWorkmotor.USUARIOS.FirstOrDefault(x => x.Id == id));
        }
        public Usuario PorLogin(string id)
        {
            return (_bdSiteWorkmotor.USUARIOS.FirstOrDefault(x => x.Email == id));
        }
    }
}
