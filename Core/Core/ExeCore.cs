﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Core
{
    public class ExeCore
    {
        private readonly BdSponsorEntities _db = new BdSponsorEntities();

        public Executaveis RetornarExePorIdPatrocinador(int? idPatrocinador)
        {
            return _db.EXECUTAVEIS.FirstOrDefault(e => e.PatrocinadorId == idPatrocinador && e.Tipo == 1);
        }

        public Executaveis RetornarAtualizadorPorIdPatrocinador(int? idPatrocinador)
        {
            return _db.EXECUTAVEIS.FirstOrDefault(e => e.PatrocinadorId == idPatrocinador && e.Tipo == 2);
        }
    }
}
