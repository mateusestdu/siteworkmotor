﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Core
{
    public class SmsHistoricoCore
    {
        private  readonly BdSponsorEntities _db = new BdSponsorEntities();

        public SmsHistorico ObterPorId(int id)
        {
            return _db.SMSHISTORICOS.FirstOrDefault(sh => sh.Id == id);
        }

        public SmsHistorico ObterPorClienteTelefone(int clienteId, string clienteTelefone)
        {
            return _db.SMSHISTORICOS.FirstOrDefault(sh => sh.ClienteId == clienteId && sh.Celular == clienteTelefone && sh.DataAtivacao == null);
        }

        public string Inserir(SmsHistorico historico)
        {
            try
            {
                _db.SMSHISTORICOS.Add(historico);
                _db.SaveChanges();
                return "Sucesso";
            }
            catch (Exception e)
            {
                return "Erro";
            }
        }

        public bool Atualizar(SmsHistorico sh)
        {
            try
            {
                _db.SMSHISTORICOS.Attach(sh);
                _db.Entry(sh).State = EntityState.Modified;
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
