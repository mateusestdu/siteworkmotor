﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Core
{
    public class ContatoCore
    {
        private readonly BdSiteWorkmotorEntities _bdSiteWorkmotor = new BdSiteWorkmotorEntities();

        public Contato AddContato(Contato add)
        {
            add.DataContato = DateTime.Now;
            _bdSiteWorkmotor.Set<Contato>().Add(add);
            _bdSiteWorkmotor.SaveChanges();
            return add;

        }
        public List<Contato> TodosContatos()
        {
            return _bdSiteWorkmotor.CONTATOS.ToList();
        }

        public bool SalvarCv(int id, string anexoContato)
        {
            var contato = _bdSiteWorkmotor.CONTATOS.FirstOrDefault(x => x.Id == id);
            if (contato == null) return false;
            contato.UrlCv = anexoContato;
            _bdSiteWorkmotor.Entry(contato).State = EntityState.Modified;
            _bdSiteWorkmotor.CONTATOS.Attach(contato);
            _bdSiteWorkmotor.SaveChanges();
            return true;
        }

        public void AtualizarContatosVisualizados(List<Contato> contatos)
        {
            foreach (var contato in contatos.Where(x => x.DataVisualizacao == null))
            {
                contato.DataVisualizacao = DateTime.Now;
                _bdSiteWorkmotor.Entry(contato).State = EntityState.Modified;
                _bdSiteWorkmotor.CONTATOS.Attach(contato);
                _bdSiteWorkmotor.SaveChanges();
            }
        }
    }

}
