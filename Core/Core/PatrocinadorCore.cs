﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Core
{
    public class PatrocinadorCore
    {
        private readonly BdSponsorEntities _db = new BdSponsorEntities();

        public List<Patrocinador> RetornarTodos()
        {
            return _db.PATROCINADORES.Where(e => e.Habilitado == true).OrderBy(e => e.Id).ToList();
        }

        public Patrocinador RetornarPorId(int? idPatrocinador)
        {
            return _db.PATROCINADORES.FirstOrDefault(p => p.Id == idPatrocinador);
        }

        public List<Patrocinador> RetornarPorDescricaoList(string dist)
        {
            return _db.PATROCINADORES.Where(d => d.NomeFantasia == dist).ToList();
        }

        public Patrocinador RetornarPorDescricao(string dist)
        {
            return _db.PATROCINADORES.FirstOrDefault(d => d.NomeFantasia == dist);
        }
    }
}
