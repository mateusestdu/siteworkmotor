﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Core
{
    public class LogContatoCore
    {
        private readonly BdSponsorEntities _db = new BdSponsorEntities();

        public bool Inserir(LogContatoPatrocinador logContato)
        {
            try
            {
                _db.LOGCONTATOPATROCINADOR.Add(logContato);
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                var a = e.Message;
                return false;
            }
        }
    }
}
