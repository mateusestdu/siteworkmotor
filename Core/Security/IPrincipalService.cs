﻿using System.Security.Principal;

namespace Domain.Security
{
    public interface IPrincipalService
    {
        IPrincipal GetCurrent();
    }
}
