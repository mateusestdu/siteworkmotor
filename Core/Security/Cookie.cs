﻿namespace Domain.Security
{
    public class Cookie
    {
        public long Id { get; set; }
        public string Login { get; set; }
    }
}
