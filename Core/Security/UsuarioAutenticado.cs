﻿using System;
using System.Web;
using Core.Core;

namespace Core.Security
{
    public class UsuarioAutenticado
    {
        private HttpContext _context = HttpContext.Current;
        private readonly UsuarioCore _usuarioCore = new UsuarioCore();

        public void SetHttpContext(HttpContext current)
        {
            _context = HttpContext.Current;
        }
        public void SetUsuario(Usuario usuario)
        {
            _context.Session["currentUser"] = usuario;
        }

        public Usuario Usuario
        {
            get
            {
                if (_context.Session["currentUser"] != null)
                    return (Usuario)_context.Session["currentUser"];
                if (!String.IsNullOrEmpty(_context.User.Identity.Name))
                {
                    try
                    {
                        var usu = _usuarioCore.PorId(Convert.ToInt32(_context.User.Identity.Name));
                        if (usu != null) _context.Session["currentUser"] = usu;
                    }
                    catch { }
                }

                    return (Usuario)_context.Session["currentUser"];
                }
            }
        }
    }
