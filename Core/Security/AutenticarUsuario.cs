﻿using System.Web.Mvc;

namespace Core.Security
{
    public class AutenticarUsuario : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (AuthorizeCore(filterContext.HttpContext))
            {
                base.OnAuthorization(filterContext);
            }
            else
            {
                filterContext.Result = new RedirectResult("~/Login/Index");
            }
        }
    }
}
