﻿using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace Domain.Security
{
    public class PrincipalService
    {
        private readonly HttpContext _context;
        
        public PrincipalService(HttpContext context)
        {
            _context = context;
        }

        public IPrincipal GetCurrent()
        {
            var user = _context.User;

            if (user is Principal) return user;

            if (user == null || !user.Identity.IsAuthenticated || !(user.Identity is FormsIdentity)) return null;
            var id = (FormsIdentity)user.Identity;

            var ticket = id.Ticket;

            if (FormsAuthentication.SlidingExpiration)
                ticket = FormsAuthentication.RenewTicketIfOld(ticket);

            var fid = new Identity(ticket);
            return new Principal(fid);
        }
    }
}
