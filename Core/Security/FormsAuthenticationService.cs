﻿using System;
using System.Web;
using System.Web.Security;
using Domain.Security;
using Newtonsoft.Json;

namespace Core.Security
{
    public class FormsAuthenticationService
    {

        private readonly HttpContext _context;

        public FormsAuthenticationService(HttpContext context)
        {
            _context = context;
        }

        public void SignIn(Usuario usuarioempresa, bool createPersistentCookie)
        {
            if (usuarioempresa == null) throw new ArgumentNullException(nameof(usuarioempresa));

            var cookie = new Cookie {Id = usuarioempresa.Id, Login = usuarioempresa.Email};
            var userData = JsonConvert.SerializeObject(cookie);
            var ticket = new FormsAuthenticationTicket(1, usuarioempresa.Id.ToString(), DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout), createPersistentCookie, userData);
            var encTicket = FormsAuthentication.Encrypt(ticket);
            var httpCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket)
            {
                Expires = DateTime.Now.Add(FormsAuthentication.Timeout)
            };
            FormsAuthentication.SetAuthCookie(usuarioempresa.Nome,true);
            _context.Response.Cookies.Add(httpCookie);
            _context.Session["currentUser"] = usuarioempresa;

        }
        
        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }
}
