﻿using System;
using System.Security.Principal;
using System.Web.Security;
using Core;
using Newtonsoft.Json;

namespace Domain.Security
{
     public class Identity : IIdentity
     {
         public Identity(FormsAuthenticationTicket ticket)
         {
             if (ticket == null) { throw new ArgumentNullException(nameof(ticket)); }

             var data = JsonConvert.DeserializeObject<Cookie>(ticket.UserData);
             if (data == null) throw new Exception("ticket is null");

             Id = data.Id;
             Login = data.Login;
         }

         public Identity(Usuario usuario)
         {
             if (usuario == null) throw new ArgumentNullException(nameof(usuario));
             Id = usuario.Id;
             Login = usuario.Id.ToString();
             Name = usuario.Id.ToString();
         }

         public long Id { get; set; }
         public string Login { get; set; }
         public string Name { get; set; }
         public string AuthenticationType => "GuiaForms";
         public bool IsAuthenticated => !(Id == 0 && string.IsNullOrWhiteSpace(Login));
     }
}
