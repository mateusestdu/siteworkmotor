﻿using System.Security.Principal;

namespace Domain.Security
{
    public class Principal : IPrincipal
    {
        private readonly Identity _identity;

        public Principal(Identity identity)
        {
            _identity = identity;
        }

        public bool IsInRole(string role) { return true; }

        public IIdentity Identity => _identity;
    }
}
